# Table of Contents

- [Table of Contents](#Table-of-Contents)
  - [About](#About)

## About

This is an example of an empty implementation of a backend for the tango HDB++ project.
The overview is in the main project [README](../README.md).

